package com.spring.boot.autoconfigure;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @author wanghaicheng
 * @since 1.0.0
 */
@Slf4j
@Data
public class RDBProperty {
    /**
     * index 仓库索引
     */
    private int index;
    /**
     * host 地址
     */
    private String host;
    /**
     * 用户名
     */
    private String username;
    /**
     * 端口
     */
    private int port;
    /**
     * 密码
     */
    private String password;

    @Override
    public String toString() {
        return "RDBProperty{" +
                ", host='" + host + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
