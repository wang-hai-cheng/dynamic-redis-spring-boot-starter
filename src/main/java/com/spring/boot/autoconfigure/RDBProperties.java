package com.spring.boot.autoconfigure;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author wanghaicheng
 * @since 1.0.0
 */
@Slf4j
@Data
@ConfigurationProperties(prefix = RDBProperties.PREFIX)
public class RDBProperties {
    public static final String PREFIX = "spring.redis";

    Map<String, RDBProperty> dynamic = new LinkedHashMap<>();

    @Override
    public String toString() {
        return "RDBProperties{" +
                "dynamic=" + dynamic +
                '}';
    }
}
