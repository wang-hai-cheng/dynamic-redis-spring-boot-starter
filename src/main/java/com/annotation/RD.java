package com.annotation;


import java.lang.annotation.*;

/**
 * 切换数据源的核心注释。它可以在类上进行注释
 *
 * @author wanghaicheng
 * @since 1.0.0
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RD {

    /**
     * 数据库配置名
     *
     * @return 要切换的redis数据库配置名
     */
    String value();
}
