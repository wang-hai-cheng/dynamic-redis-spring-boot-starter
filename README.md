<p align="center">
	<strong>一个基于springboot的redis快速集成多数据源的启动器</strong>
</p>

其支持 **Jdk 1.8+, SpringBoot 2.1.18.RELEASE+

1. 编译此项目到本地  
   (1)执行mvn install<br>
   (2)在需要使用的项目加上依赖<br>
   ```html
   <groupId>wanghaicheng</groupId>
   <artifactId>dynamic-redis-spring-boot-starter</artifactId>
   <version>1.0-SNAPSHOT</version>   
   ```
2. 配置数据源。

```yaml
spring:
  redis:
    dynamic:
      datasourceName_1: #注意此处需要与  @RD("datasourceName_1")  注解内容相对应
        index: 1
        host: 127.0.0.1
        port: 6379
        password: 123456
      datasourceName_2: #注意此处需要与  @RD("datasourceName_2")  注解内容相对应
        index: 2
        host: 127.0.0.1
        port: 6379
        password: 123456
```

3. 使用  **@RD**  切换数据源。

**@RD** 可以注解在类上

|     注解      |                   结果                   |
| :-----------: | :--------------------------------------: |
|    没有@RD    |                默认spring-data-redis配置的数据源                |
| @RD("dsName") | dsName为具体某个库的名称 |

```java
@RD("datasourceName_1")
public interface UserRepository extends CrudRepository<User, String> {
    
}
```

有任何疑问欢迎随时提交issues或给项目评论